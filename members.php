<?

	include('template.php');
	registerCSS('/css/members.css');
	registerHeadInfo('<script type="text/javascript" 
src="http://www.cornify.com/js/cornify.js"></script>');

	/*
	 *   Attention!
	 * To modify the data for some year's group photo,
	 * look in member_data for the .json file of the
	 * year you want. The syntax should be pretty
	 * self-explanatory.
	 * Also, to give someone's name a hyperlink, edit
	 * member_data/websites.json. Just make sure that
	 * their name matches as it appears in the year's
	 * .json file
	 */

	try{
	
	$datadir = "member_data";
	$photodir = "member_photos";
	$minyear = 1995;
	$maxyear = max(array_map("intval", scandir($datadir)));

	if(isset($_GET['year'])/* && is_numeric($_GET['year'])*/){
		$year = /*(int) */$_GET['year'];
	} else {
		$year = $maxyear;
	}
	
	$json_contents = @file_get_contents("$datadir/$year.json") or raise("Could not open data file $datadir/$year.json.");
	$data = json_decode($json_contents) or raise("Error decoding data file.");
	$data = cast($data, "GroupOfPeople");
	
	$websites_contents = @file_get_contents("$datadir/websites.json") or raise("Could not open websites file $datadir/websites.json.");
	$websites = json_decode($websites_contents, true) or raise("Error decoding website file.");
	
	$imagesize = @getimagesize($photodir."/".$data->img);
	if($imagesize === FALSE) $imagesize = array(486, 288);
	registerHeadInfo("<style type='text/css' media='all'>
	#main-content {
	  width: ".($imagesize[0]+8)."px;
	}
  </style>");
  
	writeHeader('Members', 'culture');
	$headerWritten = true;	
	
?>

<div id="main-content">
	<? try { ?>

  <h3>Members of 
	<? 
	if(is_numeric($year)){
		echo $year."&#8211;".substr($year+1, 2);
	} else {
		echo $year;
	}
	?>
  </h3>
  <ul class="member-photo">
	<?
		//for($y = $minyear; $y <= $maxyear; $y++){
		$years = array_diff(scandir($datadir), array('.', '..', 'websites.json'));
		array_walk($years, create_function('&$str', '$str = substr($str, 0, -strlen(".json"));'));
		foreach($years as $y){
			if($y != $minyear) echo "\t";
			if(is_numeric($y)){
				echo "<li><a href='?year=$y'>[$y-".substr($y+1,2)."]</a></li>\n";
			} else {
				echo "<li><a href='?year=$y'>[$y]</a></li>\n";
			}
		}
	?>
  </ul>

  <img src="<?= $photodir."/".$data->img ?>" alt="Photo of Tech House Members" class="figure" width="<?= $imagesize[0] ?>" height="<?= $imagesize[1] ?>" />

  <dl>
  <?= $data->writeAllRows(); ?>
  </dl>
  
  <? 
	} catch (Exception $e){
		
		echo "<h3>Exception</h3>\n";
		echo "<h4>".$e->getFile().", line ".$e->getLine()."</h4>\n";
		echo "<p>".$e->getMessage()."</p>\n";
		echo "<pre>".nl2br($e->getTraceAsString())."</pre>";
	}
	?>
	
  </div>
  
<?
	} catch (Exception $e){
		writeHeader('Members', 'culture');
		
		echo '<div id="main-content">';
		echo "<h3>Exception</h3>\n";
		echo "<h4>".$e->getFile().", line ".$e->getLine()."</h4>\n";
		echo "<p>".$e->getMessage()."</p>\n";
		echo "<pre>".nl2br($e->getTraceAsString())."</pre>";
		echo '</div>';
	}

	writeFooter();

class Person {
	public $name;
	function output(){
		global $websites;
		if(isset($websites[$this->name])){
			return "<a href='{$websites[$this->name]}'>{$this->name}</a>";
		} else {
			return $this->name;
		}
	}
}


class GroupOfPeople {
	public $year, $img, $rows;
	
	function writeRow($row){
		$result = "\t<dt>".$row->label."</dt>\n\t\t<dd>";
		
		$result .= implode(', ', array_map(array($this, 'handlePerson'), $row->people));
		$result .= "</dd>";
		return $result;
	}
	
	function writeAllRows(){
		$result = '';
		foreach ($this->rows as $row){
			$result .= $this->writeRow($row) . "\n";
		}
		return $result;
	}
	
	function handlePerson($name){
//		$person = cast($obj, "Person");
		$person = new Person();
		$person->name = $name;
		return $person->output();
	}
	
}

function cast($object, $newType){
	$return = new $newType();
	foreach(get_object_vars($object) as $key=>$val){
		$return->$key = $val;
	}
	return $return;
}


function raise($str){
	throw new Exception($str);
}

?>
