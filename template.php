<?php

/**
* @desc This script provides functions to include the website header and footer
* @functionsyoucanuse writeHeader(), writeFooter(), registerCSS(), registerHeadInfo()
* @author ben@techhouse.org
*/


//include protection
if(basename(__FILE__) == basename($_SERVER['PHP_SELF'])){
	die('Must be included, not run standalone.');
}


/**
* @desc Lets you tell writeHeader() that there are additional CSS files (relative to the page) to load for this page
*/
$extraCSSfilenames = array();

function registerCSS($filename){
	global $extraCSSfilenames;
	array_push($extraCSSfilenames, $filename);
}

/**
* @desc Lets you tell writeHeader() to insert extra content into the head
*/
$extraHeadInfo = array();

function registerHeadInfo($newInfo){
	global $extraHeadInfo;
	array_push($extraHeadInfo, trim($newInfo));
}

/**
* @desc Writes the page header, which includes external links, the navigation and title, container DIVs, and all the top stuff
*/

function writeHeader($title = '', $navigationcolumn = ''){
	global $extraCSSfilenames, $extraHeadInfo;
	
	//TITLE
	$title = empty($title) ? "Technology House" : "Technology House | $title";
	
	//WHICH NAVIGATION COLUMN TO HIGHLIGHT
	$about = $culture = '';
	if(!empty($navigationcolumn)){
		$navigationcolumn = strtolower($navigationcolumn);
		if(!in_array($navigationcolumn, array('about', 'culture'))){
			throw new Exception("Can't highlight column '$navigationcolumn' because the only valid columns to highlight are 'about' and 'culture'. Note that 'login' pages do not use this template.");
		}
		$$navigationcolumn = " class='on'";
	}
	
	//ANY EXTRA META-INFORMATION TO INCLUDE
	$head = "";
	if(is_array($extraHeadInfo)){
		$head = implode("\n", $extraHeadInfo);
	}
	if(!empty($head)) $head .= "\n  ";
	
	//ANY EXTRA CSS FILES TO REFERENCE
	$csstext = '';
	if(is_array($extraCSSfilenames)){
		foreach ($extraCSSfilenames as $cssfilename){
			$csstext .= "  <link rel='stylesheet' href='$cssfilename' type='text/css' media='all' />\n";
		}
	}
	
	//WRITE IT
	echo <<<END
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  $head<title>$title</title>
  <link rel="stylesheet" href="/css/reset.css" type="text/css" media="all" />
  <link rel="stylesheet" href="/css/th.css" type="text/css" media="all" />
  <script src="/js/nav.js" type="text/javascript"></script>
$csstext</head>

<body>

<div id="header">
  <h1><a href="/"><img src="/images/logo.gif" alt="Technology House" /></a></h1>
  <div id="header-mid" $culture>
	<div id="header-left" $about>
	  <div id="nav">
		<div id="nav-about">
		  <h2>About</h2>
		  <ul>
			<li><a href="/events.php">Events</a></li>
			<li><a href="/tour">Tour</a></li>
			<li><a href="/faq.php">FAQ</a></li>
		  </ul>
		</div>
		<div id="nav-culture">
		  <h2>Culture</h2>
		  <ul>
			<li><a 
href="https://techhouse.org/photogallery">Photos</a></li>
			<li><a href="/quotes">Quotes</a></li>
			<li><a href="/archives.php">Archives</a></li>
		  </ul>
		</div>
		<div id="nav-login">
		  <h2>Login</h2>
		  <ul>
			<li><a href="/webmail">Email</a></li>
			<li><a href="/wiki">Wiki</a></li>
			<li><a href="/ssh">SSH</a></li>
		  </ul>
		</div>
	  </div>
	</div>
  </div>
</div>
END;
}


/**
* @desc Write the page footer, which basically just closes open tags
*/

function writeFooter(){
	echo <<<END
<div id="preload">
  <!-- preload rollover image -->
</div>
</body>
</html>
END;
}

?>
